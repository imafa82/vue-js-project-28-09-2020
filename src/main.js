import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all'
import 'bootstrap/dist/css/bootstrap.css'
import 'popper.js'
import 'jquery'
import 'bootstrap'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Card from "./components/Card";
import VueTouch from 'vue-touch';
import VueResource from 'vue-resource';

Vue.config.productionTip = false
Vue.use(VueResource);
Vue.use(VueTouch);
Vue.http.options.root = 'https://jsonplaceholder.typicode.com/';
Vue.http.options.headers = {
  'Content-type': 'application/json'
};
Vue.http.interceptors.push((request, next) => {
  if(localStorage.getItem('token')){
    request.headers.set('Authorization', 'Bearer '+localStorage.getItem('token'));
  }
  next(response => {
    if(response.status === 401){
      localStorage.removeItem('token');
      router.go();
    }
  });
});
Vue.component('app-card', Card);
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
