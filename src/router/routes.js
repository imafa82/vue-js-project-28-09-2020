import Home from '../views/Home.vue'
import Page from "../views/Page";
import User from "../views/User";
import Users from "../views/Users";
import Actors from "../views/Actors";


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/page',
    name: 'Page',
    component: Page
  },
  {
    path: '/actors',
    name: 'Actors',
    component: Actors
  },
  {
    path: '/users',
    name: 'Users',
    component: Users
  },
  {
    path: '/user/:id',
    name: 'UserDetail',
    component: User
  },
]



export default routes
